package com.kovalandoleg.myapk.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.kovalandoleg.myapk.ui.Dialog_Activity;
import com.kovalandoleg.myapk.util.MyParser;

public class SMSReceiver extends BroadcastReceiver {
    String msg = "ZALYPA : ";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d(msg, context.toString());

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            bundle = MyParser.extractValues(pdus);
        }

        if (bundle != null) {
            intent.setClass(context, Dialog_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("msg", bundle);
            context.startActivity(intent);
        }
    }
}

/*
        String smsMessageStr = "";
        for (int i = 0; i < sms.length; ++i) {
        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

        String smsBody = smsMessage.getMessageBody().toString();
        String address = smsMessage.getOriginatingAddress();

        smsMessageStr += "SMS From: " + address + "\n";
        smsMessageStr += smsBody + "\n";
        }
        Toast.makeText(context, smsMessageStr + " on receive", Toast.LENGTH_SHORT).show();

        //this will update the UI with message
        SmsActivity inst = SmsActivity.instance();
        inst.updateList(smsMessageStr);
        }
*/
