package com.kovalandoleg.myapk.ui;

import android.app.Activity;
import android.os.Bundle;

import com.kovalandoleg.myapk.R;


public class MainActivity extends Activity {

    public static Activity mainActivityInstance = null;

    public MainActivity() {
        mainActivityInstance = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
