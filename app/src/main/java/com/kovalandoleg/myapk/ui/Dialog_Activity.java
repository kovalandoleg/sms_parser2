package com.kovalandoleg.myapk.ui;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.kovalandoleg.myapk.R;

public class Dialog_Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_);


        Intent intent = this.getIntent();
        Bundle msg = intent.getBundleExtra("msg");

        /* Copy to clipboard. BAD Idea. Should change */
        ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("code", msg.getString("code"));
        clipboard.setPrimaryClip(clip);
        /* ----------------------------------------- */

        final TextView codeField = (TextView) findViewById(R.id.KOD);
        String tmp = "";
        short counter = 0;
        for (Character chr : msg.getString("code", getString(R.string.error)).toCharArray())
        {
            tmp += chr;
            counter++;
            if (counter == 4){
                tmp += "-";
                counter = 0;
            }

        }
        codeField.setText(tmp);

        final TextView cardField = (TextView) findViewById(R.id.CardL4D);
        cardField.setText(msg.getString("card", getString(R.string.error)));

        final TextView summField = (TextView) findViewById(R.id.SUMM);
        summField.setText(msg.getString("summ", getString(R.string.error)));
    }
}
