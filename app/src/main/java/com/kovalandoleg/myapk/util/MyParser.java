package com.kovalandoleg.myapk.util;

import android.os.Bundle;
import android.telephony.SmsMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nook on 11.02.16.
 */
public class MyParser {

    public static Bundle extractValues(Object[] smsMessages)
    {
        Bundle output = null;
        SmsMessage sms = SmsMessage.createFromPdu((byte[]) smsMessages[0]);
        String screenMessage;

        /*========= KOSTUL ==============*/
        String smsSender  = sms.getOriginatingAddress();
        if (!(smsSender.equals("PUMB") || smsSender.equals("+380")))
            return null;

        String smsBody    = sms.getMessageBody();
        long smsTimeStamp = sms.getTimestampMillis();
        output = parseKodPidtver(smsBody);
        return output == null ? null : output;


//      OPERACIYA PO KARTE ****5071 NA SUMMU 200.00UAH KOD POTVERJDENIYA 734466829
        }

    private static Bundle parseKodPidtver(String smsBody)
    {
        String cardNumber;
        String summ = "", KOD = "";
        int index;
        Bundle out = new Bundle();

        Pattern pattern = Pattern.compile("\\d*[.]\\d{2}");
        Matcher matcher = pattern.matcher(smsBody);
        if (matcher.find())
            out.putString("summ", matcher.group());

        pattern = Pattern.compile("\\d{9,10}");
        matcher = pattern.matcher(smsBody);
        if (matcher.find())
            out.putString("code", matcher.group());
        /*========= KOSTUL ==============*/
        else
            return null;

        index = smsBody.lastIndexOf("*");
        out.putString("card", smsBody.substring(index + 1, index + 5));

        return out;
    }
}
